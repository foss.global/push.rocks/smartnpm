import { expect, tap } from '@pushrocks/tapbundle';
import * as smartnpm from '../ts/index.js';
import { NpmRegistry } from '../ts/index.js';

let npmRegistry: smartnpm.NpmRegistry;
let verdaccioRegistry: smartnpm.NpmRegistry;
let testPackage: smartnpm.NpmPackage;

// lets test things with the standard npm registry
tap.test('should create valid instances', async () => {
  npmRegistry = new smartnpm.NpmRegistry();
  expect(npmRegistry).toBeInstanceOf(smartnpm.NpmRegistry);

  testPackage = new smartnpm.NpmPackage(npmRegistry);
  expect(testPackage).toBeInstanceOf(smartnpm.NpmPackage);
});

tap.test('should produce a valid search string and this return npmts', async () => {
  const packages = await npmRegistry.searchOnNpm({
    name: '@pushrocks/smartupdate',
  });
  expect(packages[0].name).toEqual('@pushrocks/smartupdate');
});

// lets test things with the verdaccio registry
tap.test('should create a verdaccio registry', async () => {
  verdaccioRegistry = new NpmRegistry({
    npmRegistryUrl: 'https://verdaccio.lossless.one',
  });
  expect(verdaccioRegistry).toBeInstanceOf(smartnpm.NpmRegistry);
});

tap.test('should get package from verdaccio', async () => {
  const npmPackage = await verdaccioRegistry.getPackageInfo('@pushrocks/smartupdate');
  expect(npmPackage.license).toEqual('MIT');
});

tap.test('should get a specific file from a package', async () => {
  const wantedFile = await verdaccioRegistry.getFileFromPackage(
    '@pushrocks/websetup',
    './ts/index.ts'
  );
  console.log(wantedFile.contentBuffer.toString());
});

tap.test('should get a specific file from a package', async () => {
  const wantedFiles = await verdaccioRegistry.getFilesFromPackage(
    '@pushrocks/websetup',
    'ts/'
  );
  for(const file of wantedFiles) {
    console.log(file.path);
  }
});

tap.test('should not get a nonexisting file from a package', async () => {
  const wantedFileNotThere = await verdaccioRegistry.getFileFromPackage(
    '@pushrocks/websetup',
    'ts/notthere'
  );
  expect(wantedFileNotThere).toBeNull();
});

tap.start();
