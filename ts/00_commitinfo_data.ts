/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartnpm',
  version: '2.0.3',
  description: 'interface with npm to retrieve package information'
}
