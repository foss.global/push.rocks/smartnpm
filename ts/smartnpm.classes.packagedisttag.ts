import * as plugins from './smartnpm.plugins.js';

export class PackageDisttag {
  name: string;
  targetVersion: string;

  constructor(nameArg: string, targetVersionArg: string) {
    this.name = nameArg;
    this.targetVersion = targetVersionArg;
  }
}