import * as plugins from './smartnpm.plugins.js';

export interface IVersionData {
  name: string;
  version: string;
  dependencies: { [key: string]: string };
  devDependencies: { [key: string]: string };
  dist: {
    integrity: string;
    shasum: string;
    tarball: string;
  };
}

export class PackageVersion implements IVersionData {
  public static createFromVersionData(versionDataArg: IVersionData) {
    const packageVersion = new PackageVersion();
    Object.assign(packageVersion, versionDataArg);
    return packageVersion;
  }

  name: string;
  version: string;
  dependencies: { [key: string]: string };
  devDependencies: { [key: string]: string };
  dist: {
    integrity: string;
    shasum: string;
    tarball: string;
  };
}
